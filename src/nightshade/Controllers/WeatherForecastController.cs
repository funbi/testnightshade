using Microsoft.AspNetCore.Mvc;

namespace nightshade.Controllers;

[ApiController]
[Route("api/")]
public class WeatherForecastController : ControllerBase
{
    private static readonly string[] Summaries = new[]
    {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

    private readonly ILogger<WeatherForecastController> _logger;

    public WeatherForecastController(ILogger<WeatherForecastController> logger)
    {
        _logger = logger;
    }

    [HttpGet("all")]
    public IActionResult Get()
    {
       return Ok(new{ Message = "All things bright and beeautiful"});
        
    }


    [HttpGet("status")]
    public IActionResult GetStatus()
    {
        return Ok("Api up and running");
    }
}
