FROM mcr.microsoft.com/dotnet/aspnet:7.0 as base
WORKDIR /app
# container port
EXPOSE 80
ENV ASPNETCORE_ENVIRONMENT=Development


FROM mcr.microsoft.com/dotnet/sdk:7.0 AS publish
WORKDIR /src
COPY . ./
RUN dotnet restore
 # publish to the publis hdirectory in the image
RUN dotnet publish -c Release -o publish


FROM base as final
WORKDIR /app
COPY --from=publish /src/publish .
ENTRYPOINT ["dotnet", "nightshade.dll"]