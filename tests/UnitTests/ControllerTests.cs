﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using nightshade.Controllers;

namespace UnitTests;

public class ControllerTests
{
    private readonly Mock<ILogger<WeatherForecastController>> logger = new();

    [Fact]
    public void GetAsyncShouldReturnAppropriateApiStatusMessage()
    {
        var expectedMessage = new { Message = "Api up and running" };

        var controller = new WeatherForecastController(logger.Object);

        var response = controller.GetStatus();

        var actualResult = response as OkObjectResult;


        actualResult.Should().NotBeNull();

        expectedMessage.Message.Should().BeSameAs(actualResult!.Value!.ToString());

    }



    
}
